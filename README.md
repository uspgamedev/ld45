# Nothing Matters

You have NOTHING. But NOTHING matters.
Nothing Matters is a puzzle-platformer where you cave your way up a mountain in search for your stolen THING. The THINGLESS you, however, can count on the incredible power of NOTHING!

Developed at Ludum Dare 45 where the theme was "Start with nothing".

Godot version: 3.1.1

