extends Control

class_name Main

export(PackedScene) var stage_scene
export(String, FILE) var restart_scene

var current_stage
var stage_preview
var nothing_preview

onready var locator = Locator.new(get_tree())

func _ready():
	self.current_stage = stage_scene.instance() as Space
	$stage/screen.add_child(self.current_stage)
	var stage_cursor : Cursor = self.current_stage.get_cursor()
	var nothing_map : Map = $nothing/screen/Nothing.get_map()
	stage_cursor.connect("teleported", nothing_map, "_teleport_to_nothing")
	stage_cursor.connect("teleported", self, "_swap_to_nothing")
	
	var nothing_cursor : Cursor = $nothing/screen/Nothing.get_cursor()
	var stage_map = self.current_stage.get_map()
	nothing_cursor.connect("teleported", stage_map, "_teleport_to_stage")
	nothing_cursor.connect("teleported", self, "_swap_to_stage")
	
	var player = locator.find("player")
	var item_handler = player.get_component(ItemHandler)
	stage_cursor.connect("pick_item", item_handler, "_pick_item")
	nothing_cursor.connect("pick_item", item_handler, "_pick_item")

	stage_cursor.connect("drop_item", item_handler, "_drop_item")
	nothing_cursor.connect("drop_item", item_handler, "_drop_item")

	stage_cursor.connect("use_item", item_handler, "_use_item")
	nothing_cursor.connect("use_item", item_handler, "_use_item")
	
	self.stage_preview = self.current_stage.get_preview()
	self.stage_preview.cursor = nothing_cursor
	self.stage_preview.track_path = stage_cursor.referential_path
	
	self.nothing_preview = $nothing/screen/Nothing.get_preview()
	self.nothing_preview.cursor = stage_cursor
	
	locator.get_bgm().play_stage_bgm()

func _swap_to_nothing(body, _unused):
	if body.is_in_group("player") and nothing_preview.can_transport():
		var stage_cursor = self.current_stage.get_cursor()
		body.velocity.y = 0
		stage_cursor.hide()
		stage_cursor.position = Vector2(-500, -500)
		$nothing/screen/Nothing.get_cursor().show()

func _swap_to_stage(body, _unused):
	if body.is_in_group("player") and stage_preview.can_transport():
		var nothing_cursor = $nothing/screen/Nothing.get_cursor()
		body.velocity.y = 0
		nothing_cursor.hide()
		nothing_cursor.position = Vector2(-500, -500)
		self.current_stage.get_cursor().show()
		
func restart():
	get_tree().change_scene(self.restart_scene)

func _input(event):
	if Input.is_action_just_pressed("reset"):
		restart()
		return
