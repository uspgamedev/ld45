extends Position2D

class_name Preview

export (bool) var horizontal_only = false
export (NodePath) var map_path

var cursor : Cursor
var track_path : NodePath

func _process(delta):
	if cursor.visible:
		show()
		$Target.position = cursor.diff_to_referential() * 16
		if horizontal_only:
			$Target.position.y = 0
		if has_node(track_path):
			var map = get_node(map_path)
			self.position = map.round_to_tile(get_node(track_path).position)
		if $Target/Area.get_overlapping_bodies().size() > 0:
			$Target.frame = 1
		else:
			$Target.frame = 0
	else:
		hide()

func can_transport() -> bool:
	return $Target.frame == 0
