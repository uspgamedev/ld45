extends TileMap

class_name Map

export(NodePath) var referential_path
export(float) var spawn_height = 0

func round_to_tile(pos: Vector2) -> Vector2:
	var tile: Vector2 = world_to_map(pos + Vector2(8, 8))
	return tile * 16

func _teleport_to_nothing(actor, relative_pos):
	if actor.is_inside_tree():
		var main = Locator.new(get_tree()).find("main")
		if main.nothing_preview.can_transport():
			var referential = get_node(referential_path)
			safe_node_transport(actor)
			actor.position.x = referential.position.x + relative_pos.x
			actor.position.y = spawn_height
			actor.position = round_to_tile(actor.position)
			get_parent().get_node("TransportSFX").play()

func _teleport_to_stage(actor, relative_pos):
	if actor.is_inside_tree():
		var main = Locator.new(get_tree()).find("main")
		if main.stage_preview.can_transport():
			var referential = get_node(referential_path)
			safe_node_transport(actor)
			var refpos = referential.position
			actor.position = refpos + relative_pos
			actor.position = round_to_tile(actor.position)
			get_parent().get_node("TransportSFX").play()

func safe_node_transport(actor):
	actor.get_node("CollisionShape2D").set_disabled(true)
	yield(get_tree(),"idle_frame")
	if actor.get_parent():
		actor.get_parent().remove_child(actor)
		yield(get_tree(),"idle_frame")
		self.add_child(actor)
		yield(get_tree(),"idle_frame")
		actor.get_node("CollisionShape2D").set_disabled(false)
	