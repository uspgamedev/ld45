extends Node2D

class_name Space

func get_cursor() -> Cursor:
	return $cursor as Cursor

func get_preview() -> Preview:
	return $Preview as Preview

func get_map() -> Map:
	return $map as Map

func _teleport_sfx(_unused, _unused):
	var main = Locator.new(get_tree()).find("main")
	if not main.stage_preview.can_transport():
		$DenySFX.play()
