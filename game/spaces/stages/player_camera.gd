extends Camera2D

export(NodePath) var player_path

func _process(delta):
	if has_node(self.player_path):
		var player : Actor = get_node(self.player_path) as Actor
		self.position = player.position

func shake():
	$Tween.interpolate_property(self, "offset", null, Vector2(12, 0), 0.1,
								Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")
	$Tween.interpolate_property(self, "offset", null, Vector2(-12, 0), 0.1,
								Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")
	$Tween.interpolate_property(self, "offset", null, Vector2(0, 0), 0.05,
								Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.start()