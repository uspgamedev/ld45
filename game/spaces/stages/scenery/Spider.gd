extends Node2D

export (int, 3, 12) var height

onready var web = $Web
onready var spider = $Spider
onready var spider_animation = $Spider/AnimationPlayer

onready var locator = Locator.new(get_tree())

##### Spider skittering code ################################
func _ready():
	start_timer()

func _on_Timer_timeout():
	var skitter_times = randi()%3 + 2
	match skitter_times:
		2:
			spider_animation.play("Skitter")
			yield(spider_animation, "animation_finished")
			spider_animation.play("Skitter")
		3:
			spider_animation.play("Skitter")
			yield(spider_animation, "animation_finished")
			spider_animation.play("Skitter")
			yield(spider_animation, "animation_finished")
			spider_animation.play("Skitter")
		4:
			spider_animation.play("Skitter")
			yield(spider_animation, "animation_finished")
			spider_animation.play("Skitter")
			yield(spider_animation, "animation_finished")
			spider_animation.play("Skitter")
			yield(spider_animation, "animation_finished")
			spider_animation.play("Skitter")
	yield(spider_animation, "animation_finished")
	start_timer()

func start_timer():
	var new_time = randi()%20 + 5
	new_time /= 10
	$Timer.start(new_time)
#############################################################

##### Spider moving up and down the web #####################
func _physics_process(delta):
	web.scale.y = spider.position.y - 7
	web.position.y = spider.position.y - 7
	
	var player = locator.find("player")
	# warning-ignore:narrowing_conversion
	var distance_to_player: int
	if player != null:
		distance_to_player = abs(player.position.x - self.position.x)
		distance_to_player = distance_to_player - distance_to_player%16
		distance_to_player /= 16 
	else:
		distance_to_player = 12
	
	if distance_to_player <= 2 and spider.position.y != 15 and !$Tween.is_active():
		$Tween.interpolate_property(spider, "position",
				null, Vector2(0,15), 0.7,
				Tween.TRANS_SINE, Tween.EASE_IN)
		$Tween.start()
	elif distance_to_player > 2 and spider.position.y != 16*(height - 1) - 1 and !$Tween.is_active():
		$Tween.interpolate_property(spider, "position",
				null, Vector2(0, 16*(height - 1) - 1), 0.7,
				Tween.TRANS_SINE, Tween.EASE_IN)
		$Tween.start()
#############################################################









