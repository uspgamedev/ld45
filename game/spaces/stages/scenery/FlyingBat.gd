extends KinematicBody2D



export (Vector2) var direction
export (float, 1000) var speed

onready var locator = Locator.new(get_tree())

func _ready():
	$AnimationPlayer.play("Flying")
	direction = direction.normalized()

func _physics_process(_delta):
	if direction.x > 0:
		$Sprite.flip_h = true
	else:
		$Sprite.flip_h = false
	var player = locator.find("player")
	# warning-ignore:narrowing_conversion
	var distance_to_player: int
	if player != null:
		distance_to_player = abs(player.position.x - self.position.x)
		distance_to_player = distance_to_player - distance_to_player%16
		distance_to_player /= 16 
	else:
		distance_to_player = 0
	
	if distance_to_player <= 14:
		move_and_slide(direction*speed)


