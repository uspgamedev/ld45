extends Node2D



onready var drop = $Drop
onready var drop_speed: float = 0.0

export (int, 12) var height
export (float, 1, 10) var period

func _ready():
	$Timer.start(period)
	height -= 1
	$Splash.position.y = height*16

func _physics_process(delta):
	if drop.position.y < height*16:
		drop_speed += 500*delta
		drop_speed = clamp(drop_speed, 0, 100)
		drop.move_and_slide(Vector2(0,drop_speed))
		if drop.position.y >= height*16:
			$Splash/SplashAnimation.play("Splash")
			drop.visible = false

func _on_Timer_timeout():
	$DripSource/DripAnimation.play("Drip")
	$Timer.start(period)

func on_drip_finished(anim_name):
	drop.visible = true
	drop.position.y = -6
	drop_speed = 0.0

