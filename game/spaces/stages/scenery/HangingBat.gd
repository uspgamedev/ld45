extends Sprite



func _ready():
	start_timer()

func _on_Timer_timeout():
	var flutter_times = randi()%2 + 1
	match flutter_times:
		1:
			$AnimationPlayer.play("Flutter")
		2:
			$AnimationPlayer.play("Flutter")
			yield($AnimationPlayer, "animation_finished")
			$AnimationPlayer.play("Flutter")
	yield($AnimationPlayer, "animation_finished")
	start_timer()

func start_timer():
	var new_time = randi()%20 + 5
	new_time /= 10
	$Timer.start(new_time)


