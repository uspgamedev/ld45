extends Area2D

export(PackedScene) var next_stage

func _ready():
	$AnimationPlayer.play("default")

func _on_body_entered(body):
	if body is Actor and body.has_component(Controllable):
		$Sprite.visible = false
		body.get_component(Controllable).win()
#		body.remove_child(body.get_component(Controllable))
		Locator.new(get_tree()).get_bgm().fade_out_for($Timer.wait_time)
		$WinSFX.play()
		$Timer.start()
		yield($Timer, "timeout")
		get_tree().change_scene_to(self.next_stage)
