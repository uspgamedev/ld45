extends Control

onready var locator = Locator.new(get_tree())

func _physics_process(_delta):
	var player = locator.find("player")
	var held = "NOTHING"
	if player and player.has_item:
		var name = player.get_component(ItemHandler).equipped_item.name
		var regex = RegEx.new()
		regex.compile("([^\\d]+)")
		var result = regex.search(name)
		if result:
			held = "a %s" % result.get_string()
		else:
			held = "a %s" % name
		pass
	$Label.text = "You have\n%s" % held
