extends KinematicBody2D

export(float) var speed
export(float) var gravity_acceleration

func _process(delta):
	var direction = get_directional_input()
	var velocity = direction*speed
	velocity.y += gravity_acceleration*delta
	velocity = move_and_slide(velocity)

func get_directional_input() -> Vector2:
	var directional_input = Vector2(Input.get_action_strength("ui_right") 
	- Input.get_action_strength("ui_left"), 0).normalized()
	return directional_input