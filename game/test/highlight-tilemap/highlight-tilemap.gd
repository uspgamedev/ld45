extends TileMap

func _process(delta):
	self.clear()
	var mousepos = get_local_mouse_position()
	var tilepos = self.world_to_map(mousepos)
	self.set_cell(tilepos.x, tilepos.y, 0)