extends Node

class_name BGM

func play_stage_bgm():
	if $IntroBGM.playing:
		$IntroBGM.stop()
	if not $StageBGM.playing:
		$StageBGM.play()


func play_intro_bgm():
	if $StageBGM.playing:
		$StageBGM.stop()
	if not $IntroBGM.playing:
		$IntroBGM.play()

func fade_out_for(seconds):
	#$StageBGM.volume_db -= 80
	var pos = $StageBGM.get_playback_position()
	$StageBGM.stop()
	$FadeoutTimer.wait_time = seconds
	$FadeoutTimer.start()
	yield($FadeoutTimer, "timeout")
	#$StageBGM.volume_db += 80
	$StageBGM.play(pos)
