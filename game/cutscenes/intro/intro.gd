extends Control

export (PackedScene) var first_stage

func _ready():
	Locator.new(get_tree()).get_bgm().play_intro_bgm()

func _input(event):
	if event.is_action_pressed("left_mouse_click"):
		_start_game(null)

func _start_game(_unused):
	get_tree().change_scene_to(self.first_stage)
