extends Actor

onready var jumping: bool
export(float) var jump_velocity
var player

func action(item_handler) -> bool:
	player = item_handler.player
	
	if player.is_on_floor():
		player.velocity.y -= jump_velocity
		item_handler.get_node("boots_SFX").play()
		is_jumping(player)
		
	return false

func is_jumping(player):
	jumping = true
	yield(player.get_node("controllable"), "left_click_released")
	player.velocity.y /= 3
	jumping = false