extends Actor

func action(item_handler) -> bool:
	for actor in item_handler.cursor.get_overlapping_bodies():
		var lock = actor.get_node_or_null("lock")
		if lock != null:
			lock.unlock()
			item_handler.change_item_icon_on_hud(null)
			self.queue_free()
			return true
	return false