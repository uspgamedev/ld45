extends Actor

export(float) var shield_offset
export(float) var height_offset

export(PackedScene) var invunerability_box

onready var locator = Locator.new(get_tree())
onready var player = locator.find("player")
onready var player_animator = player.get_node("Sprite/AnimationPlayer")

func action(item_handler) -> bool:
	player_animator.play("Shielding")
	
	item_handler.item_in_use = true
	var shield_instance = invunerability_box.instance()
	shield_instance.position = height_offset*Vector2.UP
	if player.get_node("Sprite").flip_h:
		shield_instance.position += shield_offset*Vector2.LEFT
	else:
		shield_instance.position += shield_offset*Vector2.RIGHT
	
	player.get_node("controllable").can_move = false
	player.add_child(shield_instance)
	shield_instance.connect("left_click_released", self, "_release_shield",[item_handler], CONNECT_ONESHOT)
		
	return false

func _release_shield(item_handler):
	if not player.is_dying:
		player.get_node("controllable").can_move = true
		
	item_handler.item_in_use = false
	if player_animator.current_animation != "Dying":
		player_animator.stop()
