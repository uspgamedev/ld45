extends Actor

func action(item_handler) -> bool:
	var player = item_handler.player
	var map = player.get_parent()
	var cursor = map.get_parent().get_cursor()
	if cursor and cursor.get_overlapping_bodies().size() == 0:
		item_handler.change_item_icon_on_hud(null)
		
		map.add_child(self)
		self.position = cursor.position
		call_deferred("enable_collision")
		item_handler.equipped_item = null
		
		return true
	else:
		item_handler.get_node("DenySFX").play()
		return false
		
func enable_collision():
	self.get_node("CollisionShape2D").set_disabled(false)
	