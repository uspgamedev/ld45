extends Sprite



onready var player = get_parent() as Actor
onready var animator = $AnimationPlayer
onready var on_floor: bool = false

func _process(delta):
	var movement = player.velocity
	
	if animator.current_animation != "Winning" and animator.current_animation != "Shielding":
		if player.is_on_floor() and animator.current_animation != "Landing":
			if abs(movement.x) > 1:
				animator.play("Walking")
			else:
				animator.play("Idle")
		elif not player.is_on_floor():
			if player.velocity.y <= 0:
				animator.play("Jumping")
			else:
				animator.play("Falling")
		
		if player.is_on_floor() and not on_floor:
			animator.play("Landing")
		
		on_floor = player.is_on_floor()
		
		if movement.x > 0:
				self.flip_h = false
		elif movement.x < 0:
			self.flip_h = true
