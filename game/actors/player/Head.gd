extends Area2D

func _physics_process(delta):
	for body in get_overlapping_bodies():
		if body.name == "Ground":
			var player = get_parent()
			if not player.is_on_floor():
				player.position.y = player.position.y + 5
			return 
