extends Area2D

export(float) var speed
export(PackedScene) var sfx_scn
export(Array, PackedScene) var hit_particles

onready var direction: float

signal hit_shield

func _ready():
	get_node("../Harm").connect("hit", self,"_on_hit") 
	self.connect("hit_shield", get_node("../Harm"),"_on_hit_shield") 

func _process(delta):
	owner.velocity.x = direction * speed

func _on_ProjectileTrajetory_body_entered(body):
	if body.is_in_group("shield"):
		emit_signal("hit_shield")
		owner.get_parent().add_child(sfx_scn.instance())
	owner.queue_free()

func _on_hit():
	for child in hit_particles:
		var particles = child.instance()
		particles.position = owner.position
		particles.emitting = true
		particles.one_shot = true
		owner.get_parent().add_child(particles)
	
	owner.queue_free()


