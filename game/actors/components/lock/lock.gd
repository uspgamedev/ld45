extends Node

const SFX_SCN = preload("res://actors/components/lock/unlock_sfx.tscn")

var unlocked = false

func unlock() -> void:
	if not unlocked:
		unlocked = true
		get_parent().add_child(SFX_SCN.instance())
		yield(get_tree().create_timer(0.1), "timeout")
		get_parent().queue_free()
