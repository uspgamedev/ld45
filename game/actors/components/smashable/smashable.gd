extends Area2D

signal smashed

func _physics_process(delta):
	for body in get_overlapping_bodies():
		if body is Actor and body.has_component(Heavy) and body != self.get_parent():
			emit_signal("smashed")
