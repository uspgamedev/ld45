extends AnimatedSprite

export(bool) var flipped = false

func _process(delta):
	var movement = (get_parent() as Actor).velocity
	
	if abs(movement.x) > 1:
		self.animation = "walking"
	else:
		self.animation = "idle"
	
	if movement.x > 0:
		self.flip_h = flipped
	elif movement.x < 0:
		self.flip_h = not flipped
	
