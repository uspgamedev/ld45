extends Node

export(float) var vision_range
export(String) var target_group

onready var locator = Locator.new(get_tree())

var target_direction:Vector2

func _physics_process(delta):
	var player = locator.find(target_group)
	if player != null:
		target_direction = player.position - owner.position
		if target_direction.length() <= vision_range:
			owner.get_node("Shooter").active = true
		else:
			owner.get_node("Shooter").active = false
		if target_direction.x <= 0:
			owner.get_node("AnimatedSprite").flip_h = false
		else:
			owner.get_node("AnimatedSprite").flip_h = true
	else:
		owner.get_node("Shooter").active = false
