extends Node

class_name Gravity

export(float, 5000) var gravity
export(float, 1000) var max_vertical_speed

const COOLDOWN = .1

onready var on_floor = false
onready var lag = 0

func _physics_process(delta):
	var owner = get_parent()
	if owner is Actor:
		owner.velocity.y += self.gravity*delta
		owner.velocity.y = clamp(owner.velocity.y, -max_vertical_speed,\
													max_vertical_speed)
		if not on_floor and owner.is_on_floor():
			on_floor = true
			lag = COOLDOWN
			$LandSFX.play()
		elif not owner.is_on_floor():
			if lag > 0:
				lag -= delta
			else:
				on_floor = false