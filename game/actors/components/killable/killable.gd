extends Node

class_name Killable

export (Array, PackedScene) var death_particles
export (AudioStream) var death_sfx

onready var locator = Locator.new(get_tree())



func kill() -> void:
	var owner: Actor = get_parent() as Actor
	
	for child in death_particles:
		var particles = child.instance()
		particles.position = owner.position
		particles.emitting = true
		particles.one_shot = true
		owner.get_parent().add_child(particles)
	
	var sound = AudioStreamPlayer.new()
	sound.stream = death_sfx
	sound.bus = "SFX"
	owner.get_parent().add_child(sound)
	sound.play()
	
	locator.find("player_camera").shake()
	
	if owner.get_component(Controllable):
		owner.is_dying = true
		owner.get_component(Controllable).restart_stage()
	else:
		owner.queue_free()


