extends Node

export(PackedScene) var projectile
export(float) var shoot_offset
export(float) var shoot_cooldown

onready var active:bool = false

func _process(delta):
	if active && $Timer.is_stopped():
		shoot()
		$Timer.start(shoot_cooldown)
		yield($Timer, "timeout")

func shoot():
	owner.get_node("AnimatedSprite").play("shoot")
	yield(owner.get_node("AnimatedSprite"), "animation_finished")
	$SFX.play()
	var projectile_instance: Actor = projectile.instance()
	owner.get_parent().add_child(projectile_instance)
	projectile_instance.creator = get_parent()
	if !owner.get_node("AnimatedSprite").flip_h:
		projectile_instance.position = owner.position + Vector2(-shoot_offset, 0)
		projectile_instance.get_node("ProjectileTrajetory").direction = -1.0
	else:
		projectile_instance.position = owner.position + Vector2(shoot_offset, 0)
		projectile_instance.get_node("ProjectileTrajetory").direction = 1.0
	owner.get_node("AnimatedSprite").play("idle")
