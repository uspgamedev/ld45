extends Area2D

signal hit()

var hit_shield = false

func _on_body_entered(body):
	call_deferred("check_if_can_kill", body)

func _on_hit_shield():
	hit_shield = true
	
func check_if_can_kill(body):
	if check_if_hit_shield(body): return
	if body is Actor and body.get_component(Killable) and not hit_shield:
		kill_body(body)
		
func check_if_hit_shield(body):
	if hit_shield:
		if get_parent().is_in_group("projectile") and body.is_in_group("player"):
			var player_sprite_flip_h = body.get_node("Sprite").flip_h
			var witch_sprite_flip_h = get_parent().creator.get_node("AnimatedSprite").flip_h
			if player_sprite_flip_h != witch_sprite_flip_h:
				kill_body(body)
				
		return true
	else:
		return false
		
func kill_body(body):
	emit_signal("hit")
	(body as Actor).get_component(Killable).kill()
