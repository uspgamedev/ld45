extends Node

class_name Walker

export(float, 1000) var max_horizontal_speed
export(float, 5000) var horizontal_acceleration
export(float, 5000) var friction
export(NodePath) var controller_path

func _physics_process(delta):
	var owner = get_parent()
	if owner is Actor and has_node(controller_path):
		var controller = get_node(controller_path)
		var input = controller.get_horizontal_input()
		var sig = sign(owner.velocity.x)
		
		if input != sig and owner.velocity.x != 0:
			owner.velocity.x -= sig*friction*delta
			if sign(owner.velocity.x) != sig and input == 0:
				owner.velocity.x = 0
		
		owner.velocity.x += input*horizontal_acceleration*delta
		owner.velocity.x = clamp(owner.velocity.x, -max_horizontal_speed,\
													max_horizontal_speed)
