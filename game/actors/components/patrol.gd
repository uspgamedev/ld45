extends Timer

export (float) var patrol_duration
export (float) var wait_duration

onready var current = -1
onready var patrolling = true

func get_horizontal_input() -> float:
	if self.patrolling:
		return self.current
	return 0.0

func _ready():
	_patrol()

func _patrol():
	self.patrolling = true
	self.wait_time = self.patrol_duration
	self.start()
	self.connect("timeout", self, "_wait", [], CONNECT_ONESHOT)

func _wait():
	self.patrolling = false
	self.current = -self.current
	self.wait_time = self.wait_duration
	self.start()
	self.connect("timeout", self, "_patrol", [], CONNECT_ONESHOT)
