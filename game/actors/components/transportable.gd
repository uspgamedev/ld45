extends Node

class_name Transportable

func transport() -> void:
	var owner = get_parent()
	
	if owner.get_node_or_null("Sprite/AnimationPlayer") != null:
		owner.get_node("Sprite/AnimationPlayer").play("Disappear")
	
	var screen = owner.get_parent()
	
	for i in range(0, 3):
		screen = screen.get_parent()
	self.get_path()
	var tree = get_tree()
	owner.get_parent().remove_child(owner)
	yield(tree,"idle_frame")
	var new_parent
	match screen.name:
		"nothing":
			new_parent = tree.get_nodes_in_group("stage_screen")[0].get_node("screen/space/map")
		"stage":
			new_parent = tree.get_nodes_in_group("nothing_screen")[0].get_node("screen/Nothing/map")
		
	new_parent.add_child(owner)
	owner.position = Vector2.ZERO