extends Node

class_name Controllable

signal left_click_released()

export(String) var current_stage

onready var can_move: bool = true
onready var locator = Locator.new(get_tree())

func get_horizontal_input() -> float:
	if can_move:
		return Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	return 0.0

func win():
	can_move = false
	var owner: Actor = get_parent() as Actor
	owner.velocity = Vector2.ZERO
	var animator = get_parent().get_node("Sprite/AnimationPlayer")
	animator.play("Winning")

func restart_stage():
	can_move = false
	var owner: Actor = get_parent() as Actor
	owner.velocity = Vector2.ZERO
	owner.collision_layer = 0
	owner.collision_mask = 0
	if owner.get_component(Gravity):
		owner.remove_child(owner.get_component(Gravity))
	owner.get_node("Sprite").visible = false
	
	var timer = Timer.new()
	add_child(timer)
	timer.start(1.5)
	yield(timer, "timeout")
	var main : Main = Locator.new(get_tree()).find("main") as Main
	main.restart()

func _physics_process(delta):
	if Input.is_action_just_released("left_mouse_click"):
		emit_signal("left_click_released")




