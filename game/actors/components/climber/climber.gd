extends Node

export(float) var hop_strength = 100

func _physics_process(delta):
	var owner : Actor = get_parent() as Actor
	if owner.is_on_wall() and owner.is_on_floor():
		owner.velocity.y -= self.hop_strength
		$SFX.play()
