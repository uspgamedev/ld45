extends Node

class_name ItemHandler
onready var player = get_parent()
onready var locator = Locator.new(get_tree())
onready var hud = locator.find("hud")
onready var item_in_use: bool = false
var map
var equipped_item
var cursor

func _physics_process(delta):
	map = player.get_parent()

func _pick_item(item):
	self.equipped_item = item
	self.player.has_item = true
	var scene = item.get_parent()
	
	item.get_node("CollisionShape2D").set_disabled(true)
	yield(get_tree(),"idle_frame")
	scene.remove_child(item)
	yield(get_tree(),"idle_frame")
	
	change_item_icon_on_hud(item)
	
func _drop_item(position):
	if item_in_use:
		return
	
	if self.equipped_item and not self.equipped_item.is_inside_tree():
		map.add_child(self.equipped_item)
		self.equipped_item.position = position
		yield(get_tree(),"idle_frame")
		
		self.equipped_item.get_node("CollisionShape2D").set_disabled(false)
		self.equipped_item = null
		self.player.has_item = false
		change_item_icon_on_hud(null)
	
func _use_item(cursor):
	assert(self.equipped_item != null)
	if item_in_use:
		return
		
	self.cursor = cursor
	var is_consumable_item: bool = self.equipped_item.action(self) 
	if is_consumable_item:
		player.has_item = false

func change_item_icon_on_hud(item):
	hud.get_node_or_null("EquippedItemIcon").change_item_icon(item)