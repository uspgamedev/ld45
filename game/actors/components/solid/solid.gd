extends Node

func _physics_process(delta):
	var owner = get_parent()
	var map = owner.get_parent() as Map
	if map:
		var rounded = map.round_to_tile(owner.position)
		owner.position.x = rounded.x
