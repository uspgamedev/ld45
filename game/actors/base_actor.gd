extends KinematicBody2D
class_name Actor

onready var velocity := Vector2()
onready var has_item = false
onready var is_dying = false
var creator

func _physics_process(delta):
	velocity = move_and_slide(velocity, Vector2(0, -1))

func has_component(type: Script) -> bool:
	for child in self.get_children():
		if child is type:
			return true
	return false

func get_component(type: Script) -> Node:
	for child in self.get_children():
		if child is type:
			return child
	return null
