extends Object

class_name Locator

var tree: SceneTree

func _init(tree):
	self.tree = tree

func find(tag: String):
	if !self.tree.get_nodes_in_group(tag).empty():
		return self.tree.get_nodes_in_group(tag)[0]

func get_bgm() -> BGM:
	return tree.get_root().get_node("BGM") as BGM
