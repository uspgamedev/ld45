extends Area2D

class_name Cursor

signal teleported(actor, relative_pos)
signal pick_item(actor)
signal drop_item(position)
signal use_item(cursor)

const backpack_scene = preload("res://actors/backpack/backpack.tscn")

const RANGE = [
	[false, false, true, false, false],
	[false, true, true, true, false],
	[true, true, true, true, true],
	[true, true, true, true, true],
	[false, true, true, true, false],
	[false, false, true, false, false],
	[false, false, false, false, false],
]

export (NodePath) var map_path
export (NodePath) var referential_path

onready var locator := Locator.new(get_tree())

var player

func _physics_process(_dt):
	self.player = locator.find('player')
	if player and self.visible and self.has_node(map_path):
		var map: Map = get_node(map_path) as Map
		var mouse_pos: Vector2 = get_global_mouse_position()
		var player_pos = player.position
		var player_tile = map.world_to_map(player_pos + Vector2(8,8))
		var mouse_tile = map.world_to_map(mouse_pos + Vector2(8,8))
		var diff = mouse_tile - player_tile
		var nearest = find_nearest(diff)
		var rounded_player_pos = map.round_to_tile(player_pos)
		self.position = rounded_player_pos + nearest * 16

func diff_to_referential() -> Vector2:
	var referential = get_node(self.referential_path)
	var map: Map = get_node(map_path) as Map
	var ref_pos = referential.position
	var ref_tile = map.world_to_map(ref_pos + Vector2(8,8))
	var self_tile = map.world_to_map(self.position + Vector2(8,8))
	return self_tile - ref_tile

func find_nearest(tile):
	var nearest = null
	var mindist = 100
	for i in range(-3,4):
		for j in range(-2,3):
			if RANGE[3+i][2+j]:
				var ranged = Vector2(j, i)
				var dist = ranged.distance_to(tile)
				if dist < mindist:
					mindist = dist
					nearest = ranged
	return nearest
	
var handling_input = false
func _input(event):
	if handling_input:
		yield(get_tree(), "idle_frame")
		yield(get_tree(), "idle_frame")
		handling_input = false
		return
		
	if self.visible && !event.is_echo():
		if player and not player.is_dying:
			if Input.is_action_just_pressed("right_mouse_click") and not handling_input:
				handling_input = true
				if player.has_item:
					if get_overlapping_bodies().size() == 0:
						emit_signal("drop_item", self.position)
					else:
						$DenySFX.play()
					return
				else:
					detect_transportable_body()
					
			elif Input.is_action_just_pressed("left_mouse_click") && event.is_action("left_mouse_click"):
				if player.has_item:
					emit_signal("use_item", self)
				else:
					detect_pickable_body()
				return

func detect_transportable_body() -> void:
	var overlapping_bodies = self.get_overlapping_bodies()
	var overlapping_areas = self.get_overlapping_areas()
	
	for body in overlapping_bodies:
		if body.is_in_group("player"):
			for area in overlapping_areas:
				if area.name == "Head":
					return
		
		if body is Actor:
			if body.has_component(Transportable):
				var referential = get_node(self.referential_path)
				var relative_pos = self.position - get_node(map_path).round_to_tile(referential.position)
				if(abs(relative_pos.x) > 36.5): return
				
#				backpack_animation(body)
				emit_signal("teleported", body, relative_pos)
				return
					
func detect_pickable_body() -> void:
	for body in self.get_overlapping_bodies():
		if body is Actor:
			if body.has_component(Pickable):
				emit_signal("pick_item", body)
				return
				
func backpack_animation(body):
	if body.is_in_group("player"):
		var stage = self.get_parent()
		if stage.name != "Nothing":
			var backpack_instance = backpack_scene.instance()
			get_parent().add_child(backpack_instance)
			backpack_instance.position = self.position
		else:
			var backpack = locator.find('backpack')
			if backpack:
				backpack.queue_free()

