extends Node2D

var player: Actor

func change_item_icon(item) -> void:
	if item == null && get_node_or_null("Sprite") != null:
		$Sprite.queue_free()
		return
	var item_sprite = item.get_node("Sprite").duplicate()
	if self.get_node_or_null("Sprite") != null:
		print("ERROR: Attempt to get item with another item equipped")
	else:
		self.add_child(item_sprite)
