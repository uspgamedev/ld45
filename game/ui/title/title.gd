extends Control

export (PackedScene) var first_stage

onready var clicked = false

func _ready():
	Locator.new(get_tree()).get_bgm().play_stage_bgm()

func _input(event):
	if not clicked and event.is_action_pressed("left_mouse_click"):
		clicked = true
		$StartSFX.play()
		$AnimationPlayer.play("blink")
		yield($AnimationPlayer, "animation_finished")
		get_tree().change_scene_to(self.first_stage)
